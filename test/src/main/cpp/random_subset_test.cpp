// random_subset_test.cpp

#include <iostream>
#include <random>
#include <boost/random.hpp>

#include "gtest/gtest.h"

//#include "_gcc_warnings.h"

#include "random_subset.h"

std::ostream& operator<<(std::ostream& o, const std::vector<size_t>& v)
{
	o << "[";
	for (auto it = v.begin(); it != v.end(); ++it) {
		o << *it;
		if (it + 1 < v.end()) {
			o << ", ";
		}
	}
	return o << "]";
}

std::ostream& operator<<(std::ostream& o, const std::vector<bool>& v)
{
	o << "[";
	for (auto it = v.begin(); it != v.end(); ++it) {
		o << (*it ? '1' : '0');
//		if (it + 1 < v.end()) {
//			o << ", ";
//		}
	}
	return o << "]";
}


const size_t trials = 10;
const size_t u = 6;
const size_t v = 3;

TEST(randomSubset, listvec)
{
	std::random_device rd;
	boost::mt19937 rng;
	rng.seed(rd());
	
	boost::uniform_01<boost::mt19937> z01(rng);

	size_t N = 2*u;
	for (size_t i = 0; i < trials; ++i) {
		std::vector<size_t> v = random_subset_listvec(N, z01);
		std::cout << v << std::endl;
	}
	EXPECT_EQ(1,1);
}

TEST(randomSubset, bitvec)
{
	std::random_device rd;
	boost::mt19937 rng;
	rng.seed(rd());
	
	boost::uniform_01<boost::mt19937> z01(rng);

	size_t N = 2*u;
	for (size_t i = 0; i < trials; ++i) {
		std::vector<bool> v = random_subset_bitvec(N, z01);
		std::cout << v << std::endl;
	}
	EXPECT_EQ(1,1);
}

TEST(randomKSubset, listvec)
{
	std::random_device rd;
	boost::mt19937 rng;
	rng.seed(rd());
	
	boost::uniform_01<boost::mt19937> z01(rng);

	size_t N = 2*u;
	size_t K = 2*v;
	for (size_t i = 0; i < trials; ++i) {
		std::vector<size_t> v = random_k_subset_listvec(K, N, z01);
		std::cout << v << std::endl;
	}
	EXPECT_EQ(1,1);
}

TEST(randomKSubset2, listvec)
{
	std::random_device rd;
	boost::mt19937 rng;
	rng.seed(rd());
	
	boost::uniform_01<boost::mt19937> z01(rng);

	size_t N = 2*u;
	size_t K = 3;//2*v;
	
	std::vector<size_t> v(K);

	for (size_t i = 0; i < trials; ++i) {
		random_k_subset2(v, K, N, z01);
		std::cout << v << std::endl;
	}
	EXPECT_EQ(1,1);
}

