// powerset_test.cpp

#include <iostream>

#include "powerset.h"
#include "gtest/gtest.h"

/*
std::ostream& operator<<(std::ostream& o, const std::vector<bool>& p)
{
    o << '{';
    for (auto it = p.begin(); it != p.end(); ++it) {
        o << *it;
        if (it + 1 < p.end()) o << ", ";
    }
    return o << '}';
}

std::ostream& operator<<(std::ostream& o, const std::vector<size_t>& p)
{
    o << '{';
    for (auto it = p.begin(); it != p.end(); ++it) {
        o << *it;
        if (it + 1 < p.end()) o << ", ";
    }
    return o << '}';
}
*/

size_t enumerate_powerset(n_set_sequence& ps)
{
    size_t count = 0;
    for (auto it = ps.begin(); it != ps.end(); ++it) {
		++count;
    }
	return count;
}

// powersets of [12]
const size_t set_size = 16;

TEST(graycodePowerset, cardinalityTest)
{
	powerset_gray_order g(set_size);
	EXPECT_EQ(enumerate_powerset(g), 1ULL << set_size);
}

TEST(binaryPowerset, cardinalityTest)
{
	powerset_bin_order g(set_size);
	EXPECT_EQ(enumerate_powerset(g), 1ULL << set_size);
}

TEST(lexicographicPowerset, cardinalityTest)
{
	powerset_lex_order g(set_size);
	EXPECT_EQ(enumerate_powerset(g), 1ULL << set_size);
}

// todo: more tests