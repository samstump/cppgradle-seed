// run_tests.cpp

#include "gtest/gtest.h"


int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


/*
#include <iostream>
#include <random>
#include <boost/random.hpp>

#include "random_subset.h"
#include "combalg_utility.h"

int main()
{
	std::random_device rd;
	boost::mt19937 rng;
	rng.seed(rd());
	boost::uniform_01<boost::mt19937> z01(rng);
	
	for (int i = 0; i < 10; ++i) {
		std::vector<bool> v = random_subset_bitvec(25, z01);	
		std::cout << v << std::endl;
	}
	for (int i = 0; i < 10; ++i) {
		std::vector<size_t> v = random_subset_listvec(25, z01);
		std::cout << v << std::endl;
	}
	for (int i = 0; i < 25; ++i) {
		std::vector<size_t> v = random_k_subset_listvec(3, 8, z01);
		std::cout << v << std::endl;
		std::cout << set_adapter::to_bitvec(v, 8) << std::endl;
	}
	
}
*/
