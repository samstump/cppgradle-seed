// k_subset_test.cpp

#include <iostream>

#include "k_subsets.h"

#include "gtest/gtest.h"

size_t N_choose_K(size_t n, size_t k)
{
	size_t numer = 1;
	size_t denom = 1;
	while (k > 0) {
		numer *= n;
		denom *= k;
		--n; --k;
	}
	return numer / denom;
}

size_t enumerate_k_subsets(n_set_sequence& ps)
{
    size_t count = 0;
    for (auto it = ps.begin(); it != ps.end(); ++it) {
		++count;
    }
	return count;
}


const size_t u = 6;
const size_t v = 3;

TEST(kSubsetLexOrder, cardinalityTestee)
{
	size_t N = 2*u;
	size_t K = 2*v;
	all_k_subsets_lex_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}


TEST(kSubsetLexOrder, cardinalityTestoe)
{
	size_t N = 2*u + 1;
	size_t K = 2*v;
	all_k_subsets_lex_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}

TEST(kSubsetLexOrder, cardinalityTestoo)
{
	size_t N = 2*u + 1;
	size_t K = 2*v + 1;
	all_k_subsets_lex_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}

TEST(kSubsetLexOrder, cardinalityTesteo)
{
	size_t N = 2*u;
	size_t K = 2*v + 1;
	all_k_subsets_lex_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}


TEST(kSubsetRevdoorOrder, cardinalityTestee)
{
	size_t N = 2*u;
	size_t K = 2*v;
	all_k_subsets_revdoor_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}

TEST(kSubsetRevdoorOrder, cardinalityTestoe)
{
	size_t N = 2*u + 1;
	size_t K = 2*v;
	all_k_subsets_revdoor_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}

TEST(kSubsetRevdoorOrder, cardinalityTesteo)
{
	size_t N = 2*u;
	size_t K = 2*v + 1;
	all_k_subsets_revdoor_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}

TEST(kSubsetRevdoorOrder, cardinalityTestoo)
{
	size_t N = 2*u + 1;
	size_t K = 2*v + 1;
	all_k_subsets_revdoor_order g(N, K);
	EXPECT_EQ(enumerate_k_subsets(g), N_choose_K(N, K));
}
