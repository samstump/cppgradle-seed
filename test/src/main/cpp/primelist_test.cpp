// primelist_test.cpp

#include <iostream>
#include <random>
#include <boost/random.hpp>

#include "gtest/gtest.h"

//#include "_gcc_warnings.h"

#include "primelist.h"

/*
std::ostream& operator<<(std::ostream& o, const std::vector<size_t>& a)
{
	o << '[';
	for (auto it = a.begin(); it != a.end(); ++it) {
		o << *it;
		if (it + 1 < a.end()) {
			o << ", ";
		}
	}
	return o << ']';
}
*/

TEST(primeList, primeCount)
{
	primelist primes(0xFFFF);
	std::cout << primes.count() << " primes <= " << primes.max_N() << ", highest prime: " << primes.last_prime() << std::endl;
	
	EXPECT_EQ(65521ULL,primes.last_prime());
}

#if 0
void test(size_t x, const primelist& primes)
{
	const primelist::factor_result result = primes.factor(x);
	std::cout << "The number of divisors of " << x << " is " << result.num_divisors << std::endl;
	std::cout << "factors: " << result.factor_list << " (" << result.factor_list.size() << ")" << std::endl;
}


int main()
{
	/*
	const size_t N = 0xFFFFFFFF;
	std::cout << "Initializing prime list...";
	std::cout.flush();
	auto t0 = boost::posix_time::microsec_clock::local_time();
	primelist primes(N);
	auto t = boost::posix_time::microsec_clock::local_time();
	auto elapsed = t - t0;
	double elap_sec = elapsed.total_microseconds() / 1.0e6;
	std::cout << "...done!  " << std::endl;
	std::cout << "N = " << N << ", " << elap_sec << " s. " << N / elap_sec << " per sec" << std::endl;
//	for (auto it = prime_list.begin(); it != prime_list.end(); ++it) {
//		std::cout << *it << ", ";
//	}
//	std::cout << std::endl;

	test(12, primes);
	test(13, primes);
	test(14, primes);
	test(15, primes);	
	
	std::cout << primes.count() << " : " << primes.max() << std::endl;
	*/
/*
	for (size_t i = 0; i <= 100; ++i) {
		std::cout << i << " nth prime: " << primes.nth_prime(i) << std::endl;
	}
	size_t c = 0;
	for (size_t i = 0; i <= 541; ++i) {
		if (primes.is_prime(i)) {
			std::cout << c++ << ", " << i << std::endl;
		}
	}
*/
//	primes.write("primes.bin");
	
	auto t0 = win32::nanosecond_clock::now();

	primelist p2("primes.bin");
	
	auto t = win32::nanosecond_clock::now();
	std::chrono::duration<double> elapsed = t - t0;
	std::cout << "primes <= " << p2.max_N() << std::endl;
	std::cout << "elapsed: " << elapsed.count() << " sec" << std::endl;
	std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count() << " ns" << std::endl;

	
	test(12, p2);
	test(13, p2);
	test(14, p2);
	test(15, p2);	
	test(2112, p2);
	test(4294967291,p2);
	test(4294967292,p2);
	test(3628800,p2);
	
	std::cout << p2.count() << " : " << p2.last_prime() << " : " << p2.max_N() << std::endl;
	
}
#endif