#pragma once

// n_set_sequence.h

#ifndef n_set_sequence_h_A79A163D_C3CE_4227_B7FB57EBEB48ED64
#define n_set_sequence_h_A79A163D_C3CE_4227_B7FB57EBEB48ED64

#include <vector>
#include "iterator_exceptions.h"
#include "sequence_generator.h"
#include "set_adapter.h"

class n_set_sequence : public sequence_generator {
    public:
        n_set_sequence(size_t n) : sequence_generator(), N(n), bitvec(N, false), listvec(N, 0), last_subset(false)
        {
            listvec.reserve(bitvec.size());
        }

        virtual ~n_set_sequence()
        {
        }
		
        const std::vector<bool>& get_bitvec() const
        {
            return bitvec;
        }

        const std::vector<size_t>& get_listvec() const
        {
            return listvec;
        }
	
        size_t size() const
        {
            return bitvec.size();
        }

        virtual void next() = 0;

	private:
		size_t N;
		
    protected:
        std::vector<bool> bitvec;
        std::vector<size_t> listvec;
        bool last_subset;
};

#endif // header

