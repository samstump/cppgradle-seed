// sequence_generator.h

#pragma once
#ifndef sequence_generator_h_66DD3ED6_6DA6_43D2_B4191A484BB6E80A
#define sequence_generator_h_66DD3ED6_6DA6_43D2_B4191A484BB6E80A

class sequence_generator {
    public:
        class iterator : public std::iterator<std::forward_iterator_tag, std::vector<bool>> {
            public:
                iterator() : ref_set(nullptr), at_end(false)
                {
                }

                iterator(const iterator& other) : ref_set(other.ref_set)
                {
                }

                iterator(sequence_generator* p, bool end) : ref_set(p), at_end(end)
                {
                }

                iterator& operator=(const iterator& other)
                {
                    if (this != &other) {
                        ref_set = other.ref_set;
                        at_end = other.at_end;
                    }
                    return *this;
                }

                ~iterator()
                {
                }

                const sequence_generator& operator*() const
                {
                    if (ref_set == nullptr) {
                        throw iterator_not_dereferenceable_exception();
                    }
                    return *ref_set;
                }

                iterator operator++()
                {
                    if (ref_set != nullptr) {
                        ref_set->next();
                        at_end = ref_set->done();
                    }
                    return *this;
                }

                iterator operator++(int)
                {
                    iterator it(*this); operator++(); return it;
                }

            private:

                sequence_generator* ref_set;
                bool at_end;

            friend bool operator==(const iterator& a, const iterator& b)
            {
                return a.ref_set == b.ref_set && (*a).index() == (*b).index() && a.at_end == b.at_end;
            }

            friend bool operator!=(const iterator& a, const iterator& b)
            {
                return !(a == b);
            }

        };

        sequence_generator() : element_refid(0), _done(false)
        {
        }

        virtual ~sequence_generator()
        {
        }

        iterator begin()
        {
            return iterator(this, false);
        }

        iterator end()
        {
            return iterator(this, true);
        }

        virtual bool done() const
        {
            return _done;
        }
		
        size_t index() const
        {
            return element_refid;
        }

        virtual void next() = 0;
		
    protected:
		size_t element_refid;
        bool _done;

};

#endif // header
