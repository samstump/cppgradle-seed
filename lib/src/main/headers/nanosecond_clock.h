// nanosecond_clock.h

#pragma once
#ifndef nanosecond_clock_7517423A_1C36_4EDD_B9E098E48CE79C05
#define nanosecond_clock_7517423A_1C36_4EDD_B9E098E48CE79C05

#ifdef WINDOWS

#include <windows.h>

namespace win32 {
	struct nanosecond_clock
	{
		typedef long long                               rep;
		typedef std::nano                               period;
		typedef std::chrono::duration<rep, period>      duration;
		typedef std::chrono::time_point<nanosecond_clock>   time_point;
		static const bool is_steady = true;

		static time_point now();
	};

	namespace
	{
		const long long g_Frequency = []() -> long long 
		{
			uint64_t frequency;
			QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
			return frequency;
		}();
	}
}

#endif // WINDOWS

#endif // header

