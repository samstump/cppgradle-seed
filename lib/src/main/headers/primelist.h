// primelist.h

#include <vector>
#include <cmath>
#include <unordered_map>
#include <fstream>

#pragma once
#ifndef primelist_h_7293CE1D_7C86_4631_9778AF4266CD4E44
#define primelist_h_7293CE1D_7C86_4631_9778AF4266CD4E44

class primelist {
	public:
		struct factor_result {
			size_t num_divisors;
			std::vector<size_t> factor_list;
		};
		
		primelist(size_t n) : N(n)
		{
			prime_list.reserve(N);
			for (size_t x = 2; x <= N; ++x) {
				bool is_prime = true;
				size_t idx = 0;
				while (is_prime && prime_list[idx]*prime_list[idx] <= x && idx < prime_list.size())
				{
					if (x % prime_list[idx] == 0) {
						is_prime = false;
					}
					++idx;
				}
				if (is_prime) {
					prime_list.push_back(x);
				}
			}
			prime_bits.resize(1 + ((prime_list.back()-3) / 2 / 64), 0);
			for (auto it = prime_list.begin() + 1; it != prime_list.end(); ++it) {
				// skip over 2, start at begin + 1
				size_t bit_idx = (*it - 3) / 2;
				size_t word_idx = bit_idx / 64;
				size_t bit_offset = bit_idx % 64;
				prime_bits[word_idx] |= (1ULL << bit_offset);
			}
			std::cout << prime_bits.size() * 8;
		}
		
		primelist(const char* filename)
		{
			std::ifstream ifs(filename, std::ios::in | std::ios::binary);
			ifs.read((char*)&N, sizeof(N));
			size_t nwords = 0;
			ifs.read((char*)&nwords, sizeof(nwords));
			prime_bits.resize(nwords, 0);
			for (auto it = prime_bits.begin(); it != prime_bits.end(); ++it) {
				size_t data = 0;
				ifs.read((char*)&data, sizeof(data));
				*it = data;
			}
			prime_list.reserve(N);
			prime_list.push_back(2);
			for (size_t x = 3; x <= N; ++x) {
				if (is_prime(x)) {
					prime_list.push_back(x);
				}
			}
			ifs.close();
		}
		
		const factor_result factor(size_t x) const
		{
			factor_result result;
			std::unordered_map<size_t, size_t> factor_map = get_factor_map(x);
			result.num_divisors = 1;
			for (auto it = factor_map.cbegin(); it != factor_map.cend(); ++it) {
				size_t m = (*it).second;	// multiplicity
				size_t p = (*it).first;		// prime factor
				result.num_divisors *= (m + 1);
				while (m--) {
					result.factor_list.push_back(p);
				}
			}
			return result;
		}	
				
		const size_t count() const
		{
			return prime_list.size();
		}
		
		const size_t max_N()
		{
			return N;
		}
		
		const size_t last_prime() const
		{
			return prime_list.back();
		}
		
		const bool is_prime(size_t x) const
		{
			if (x == 2) return true;
			if (x < 2 || x % 2 == 0) return false;
			size_t b = (x-3)/2;
			size_t idx = b / 64;
			size_t offset = b % 64;
			return (prime_bits[idx] & (1ULL << offset));
		}
		
		const size_t nth_prime(size_t n) const
		{
			if (n < prime_list.size()) {
				return prime_list[n];
			}
			return 0;
		}
		
		const bool write(const char* filename) const
		{
			std::ofstream ofs(filename, std::ios::out | std::ios::binary);
			size_t word = prime_bits.size();
			ofs.write((char*)&N, sizeof(N));		// primes \leq N
			ofs.write((char*)&word, sizeof(word));
			for (auto it = prime_bits.begin(); it != prime_bits.end(); ++it) {
				word = *it;
				ofs.write((char*)&word, sizeof(word));	
			}
			ofs.close();
			return true;
		}
	
	private:
		size_t N;
		std::vector<size_t> prime_list;
		std::vector<uint64_t> prime_bits;
		
		const std::unordered_map<size_t, size_t> get_factor_map(size_t x) const
		{
			std::unordered_map<size_t, size_t> factor_map;
			size_t idx = 0;
			while (x > 1 && idx < prime_list.size()) {
				size_t p = prime_list[idx];
				if (x % p == 0) {
					if (factor_map.find(p) == factor_map.end()) {
						factor_map.insert(std::make_pair(p, 0));
					}
					factor_map[p]++;
					x /= p;
				} else {
					++idx;
				}
			}
			// todo: detect error if x != 1. original x had a prime factor > N
			return factor_map;
		}
		
};

#endif // header
