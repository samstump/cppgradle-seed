// iterator_exceptions.h
#pragma once

#ifndef iterator_exceptions_h_8A2339E3_A265_4EB7_89BB9ACDA5F600E4
#define iterator_exceptions_h_8A2339E3_A265_4EB7_89BB9ACDA5F600E4

#include <stdexcept>

class iterator_not_dereferenceable_exception : public std::runtime_error
{
    public:
        iterator_not_dereferenceable_exception() :
            std::runtime_error("iterator not dereferenceable!")
        {
        }
};

#endif // header

