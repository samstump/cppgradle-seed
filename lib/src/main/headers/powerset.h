// powerset.h
#pragma once

#ifndef powerset_h_D8440C3D_AFCA_41A5_9742132B99F94908
#define powerset_h_D8440C3D_AFCA_41A5_9742132B99F94908

#include <vector>
#include "iterator_exceptions.h"
#include "n_set_sequence.h"
#include "set_adapter.h"

class powerset_bin_order : public n_set_sequence {
    public:
        powerset_bin_order(size_t N) : n_set_sequence(N) {
			set_adapter::assign_listvec(listvec, bitvec);
		}
        ~powerset_bin_order() {}

        virtual void next()
        {
            next_inner();
            set_adapter::assign_listvec(listvec, bitvec);
        }

    private:
        virtual void next_inner()
        {
            if (last_subset) {    // one past the end
                _done = true;
                return;
            }
            size_t i = 0;
            while (bitvec[i]) {
                bitvec[i] = false;
                --element_refid;
                ++i;
            }
            bitvec[i] = true;
            ++element_refid;
            last_subset = element_refid == bitvec.size();    // have we generated the last subset?
        }

};

class powerset_gray_order : public n_set_sequence {
    public:
        powerset_gray_order(size_t N) : n_set_sequence(N) {
			set_adapter::assign_listvec(listvec, bitvec);
		}
        ~powerset_gray_order() {}

        virtual void next()
        {
            next_inner();
            set_adapter::assign_listvec(listvec, bitvec);
        }

    private:
        virtual void next_inner()
        {
            if (last_subset) {    // one past the end
                _done = true;
                return;
            }
            size_t j = 0;
            if (element_refid % 2 != 0) {
                do {
                    ++j;
                } while (!bitvec[j-1]);
            }
            bitvec[j] = !bitvec[j];
            element_refid += 2 * bitvec[j] - 1;
            last_subset = (element_refid == 1 && bitvec[bitvec.size() - 1]);    // have we generated the last subset?
        }        
};

// todo: find a way to set 'jump_over_supersets' at a given point in the sequence.
class powerset_lex_order : public n_set_sequence {
    public:
        powerset_lex_order(size_t N) : n_set_sequence(N), n(N), jump_over_supersets(false), started(false)
        {
            listvec.reserve(n);
        }

        powerset_lex_order(size_t N, const std::vector<size_t>& v) : n_set_sequence(N), n(N), jump_over_supersets(false), started(false)
        {
            element_refid = v.size();
            listvec.resize(v.size());
            std::copy(v.begin(), v.end(), listvec.begin());
            set_adapter::assign_bitvec(bitvec, listvec);
        }
        
        virtual void next()
        {
            next_inner();
            set_adapter::assign_bitvec(bitvec, listvec);
            _done = started && element_refid == 0;        // one past the end, in this case {}
        }

    private:       
        void next_inner()
        {
            started = true;
            size_t is = 0;
            if (element_refid == 0) {
                if (jump_over_supersets) return;
                is = 0;
                if (!jump_over_supersets) {
                    ++element_refid;
                    listvec.push_back(-1);
                }
                listvec.back() = is;
                return;
            }
            if (listvec.back() != n-1) {
                is = listvec.back() + 1;
                if (!jump_over_supersets) {
                    ++element_refid;
                    listvec.push_back(-1);
                }
                listvec.back() = is;
                return;
            }
            --element_refid;
            listvec.pop_back();
            if (element_refid == 0) return;
            is = listvec.back() + 1;
            listvec.back() = is;
        }

    private:
        size_t n;
        bool jump_over_supersets;
        bool started;
};

#endif // header

