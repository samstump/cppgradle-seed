// k_subsets.h

#pragma once
#ifndef k_subsets_h_BED05982_719D_4D88_962F50C8D9236A14
#define k_subsets_h_BED05982_719D_4D88_962F50C8D9236A14

#include "n_set_sequence.h"

class all_k_subsets_revdoor_order : public n_set_sequence {
	public:
		all_k_subsets_revdoor_order(size_t n, size_t k) : n_set_sequence(k), N(n), K(k), in(0), out(0), _done(false)
		{
			set_adapter::assign_identity_map(listvec);
		}
		
		const std::vector<size_t>& get_listvec() const
		{
			return listvec;
		}
		
		size_t size() const
		{
			return K;
		}
		
        iterator begin()
        {
            return iterator(this, false);
        }

        iterator end()
        {
            return iterator(this, true);
        }

		bool done() const
		{
			return _done;
		}
		
		void next()
		{
			// reference: https://people.sc.fsu.edu/~jburkardt/cpp_src/combo/combo.cpp
			//		void ksubset_revdoor_successor ( int k, int n, int t[], int &rank )
			//		- removed rank, added in/out
			//		- most substantially, made it 0 indexed for storage and elements!
			// todo: if n < k, this will not terminate!
			size_t j = 0;
			for (;;) {
				if (j > 0 || (K % 2) == 0) {
					j = j + 1;
					if (K < j) {
						listvec[K-1] = K-1;
						in = K-1;
						out = N-1;
						_done = true;
						return;
					}
					if (listvec[j-1] != j-1) {
						out = listvec[j-1];
						in = listvec[j-1] - 1;
						listvec[j-1] = listvec[j-1] - 1;
						if (j > 1) {
							in = j - 2;
							listvec[j-2] = j - 2;
						}
						return;
					}
				}
				j = j + 1;
				if (j < K) {
					if (listvec[j-1] != listvec[j] - 1) {
						break;
					}
				} else {
					if (listvec[j-1] != N-1) {
						break;
					}
				}
			}
			in = listvec[j-1] + 1;
			out = listvec[j-1];
			listvec[j-1] = listvec[j-1] + 1;

			if (j > 1) {
				out = listvec[j-2];
				listvec[j-2] = listvec[j-1] - 1;
			}
			return;
		}

	private:
		size_t N;
		size_t K;
		size_t in;
		size_t out;

		bool _done;

};

class all_k_subsets_lex_order : public n_set_sequence {
	public:
		all_k_subsets_lex_order(size_t n, size_t k) : n_set_sequence(k), N(n), K(k), h(0), last_subset(false), _done(false)
		{
			set_adapter::assign_identity_map(listvec);
		}
		
		const std::vector<size_t>& get_listvec() const
		{
			return listvec;
		}
		
		size_t size() const
		{
			return K;
		}
		
		bool done() const
		{
			return _done;
		}
		
		void next()
		{
			if (last_subset) {
				_done = true;
				return;
			}
			++listvec[K-h-1];
			for (size_t t = K - h; t < K; ++t) {
				listvec[t] = listvec[t-1] + 1;
			}
			if (listvec[K-h-1] < N - h - 1) {
				// last element can be incremented, stay there...
				h = 0;
			} else {
				// last element at max, look back ...
				++h;
			}
			if (listvec[0] == N - K) {
				last_subset = true;
			}
		}
		
	private:
		size_t N;
		size_t K;
		size_t h;
		bool last_subset;
		bool _done;
};

#endif // header
