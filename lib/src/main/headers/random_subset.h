// random_subset.h
#pragma once

#ifndef random_subset_h_6489A54E_86C4_4E37_A9A2040A8B99081C
#define random_subset_h_6489A54E_86C4_4E37_A9A2040A8B99081C

#include "set_adapter.h"

template <typename T>
std::vector<bool> random_subset_bitvec(size_t N, T& rng)
{
	std::vector<bool> retval(N, false);
	for (auto it = retval.begin(); it != retval.end(); ++it) {
		*it = rng() > 0.5;
	}
	return retval;
}

template <typename T>
std::vector<size_t> random_subset_listvec(size_t N, T& rng)
{
	return set_adapter::to_listvec(random_subset_bitvec(N, rng));
}

template <typename T>
std::vector<size_t> random_k_subset_listvec(size_t K, size_t N, T& rng)
{
	std::vector<size_t> retval;
	retval.reserve(N);
	size_t i = 0;
	while (K > 0) {
		double prob = double(K)/N;
		if (rng() < prob) {
			--K;
			retval.push_back(i);
		}
		++i;
		--N;
	}
	return retval;
}

template <typename T>
void random_k_subset2(std::vector<size_t>& a, size_t K, size_t N, T& rng)
{
	size_t x = 0;
	size_t l = 0;
	size_t p = 0;
	size_t s = 0;
	size_t m = 0;
	size_t ds = 0;
	size_t r = 0;
	size_t m0 = 0;
	size_t i = 0;
	size_t w = 0;
	bool restart_loop = true;
	
	size_t c = K;

	// invariant: elements are in {1,2,3,4,5,...,n} (1 indexed)
	// storage is in a[1], a[2], ..., a[k] (also 1 indexed)
	
	// Divide the range [1,n] into k subranges, R_l = {m|(l-1)*n/k < m <= l*n/k}, l=1,2,3...,k
	// The random k-set to be chosen will consist of members B_l of the range R_l
	for (i = 0; i < K; ++i) {
		a[i] = i * N / K;
	}
	// initially, a[l] = one less than the smallest element of R_l
	for (;;) {
		// select x, a random element of {1,2,3...,n}
		x = 1 + size_t(double(N) * rng());
		
		// x lives in the l-th bin
		l = (x * K - 1) / N;
		
		// suppose we chose m members of R_l, out of q = |R_l|
		// this models the probability of rejecting x with p = m/q
		if (x <= a[l]) continue;
		
		// increase a[l], which in final state will contain the number of elements chosen from R_l
		a[l] = a[l] + 1;
		
		// do this K times
		c = c - 1;
		if (c == 0) break;
	}
	
	// now a[l] = one less than the smallest element of R_l + the number of elements to choose from R_l
	// or a[l] = one less than the smallest element of R_l + |B_l|
	// move the non-empty bins to the left of the array (a[1],a[2],...,a[p])
	p = 0;
	s = K;
	for (i = 0; i < K; ++i) {
		m = a[i];
		a[i] = 0;
		if (m != i * N / K) {
			a[p] = m;
			p = p + 1;
		}
	}

	// reserve space for each of the bins, starting from the right
	// reserve |B_l| spaces for B_l in a[1],a[2],...a[k]
	// store l in the rightmost of these spaces, and zero the others out
	for (;;) {
		l = 1 + (a[p-1] * K - 1) / N;
		ds = a[p-1] - (l-1) * N / K;
		a[p-1] = 0;
		a[s-1] = l;
		s = s - ds;
		p = p - 1;
		if (p == 0) {
			break;
		}
	}

	// again from the right, place random elements of R_l info the space reserved for B_l
	l = K;

	restart_loop = true;
	
	for (;;) {
		if (restart_loop) {
			if (a[l-1] != 0) {
				// process a new bin
				r = l;
				m0 = 1 + (a[l-1] - 1) * N / K;
				m = a[l-1] * N / K - m0 + 1;
			}
			// choose a random x from bin l
			w = size_t(double(m) * rng());
			x = m0 + w;
			i = l;
			restart_loop = false;
		}
		// check x against previously chosen elements from bin
		i = i + 1;
		if (i > r) {
			// insert x, if the l-th bin was last, return
			a[i-2] = x;
			m = m - 1;
			l = l - 1;
			if (l == 0) {
				break;
			}
			restart_loop = true;
			continue;
		}

		// increment x to jump over elements <= x
		if (x >= a[i-1]) {
			x = x + 1;
			a[i - 2] = a[i-1];
		} else {
			// insert x, if the l-th bin was last, return
			a[i-2] = x;
			m = m - 1;
			l = l - 1;
			if (l == 0) {
				break;
			}
			restart_loop = true;
		}
	}
	// re-index the set to zero based elements
	for (i = 0; i < K; ++i) {
		a[i] = a[i] - 1;
	}	
}


#endif // header
