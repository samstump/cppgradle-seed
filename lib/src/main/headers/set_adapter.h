// set_adapter.h
#pragma once

#ifndef set_adapter_h_27836F00_0395_4916_BFD5ACCD96B9E8C0
#define set_adapter_h_27836F00_0395_4916_BFD5ACCD96B9E8C0

#include <vector>

class set_adapter {
    public:
    static void assign_bitvec(std::vector<bool>& bitvec, const std::vector<size_t>& listvec)
    {
        bitvec.assign(bitvec.size(), false);
        for (auto it = listvec.begin(); it != listvec.end(); ++it) {
            bitvec[*it] = true;
        }
    }

    static std::vector<bool> to_bitvec(const std::vector<size_t>& listvec, size_t N)
    {
        std::vector<bool> bitvec(N, false);
        assign_bitvec(bitvec, listvec);
        return bitvec;
    }

    static void assign_listvec(std::vector<size_t>& listvec, const std::vector<bool>& bitvec)
    {
        listvec.clear();
        for (auto it = bitvec.begin(); it != bitvec.end(); ++it) {
            size_t idx = it - bitvec.begin();
            if (*it) listvec.push_back(idx);
        }	
    }

    static std::vector<size_t> to_listvec(const std::vector<bool>& bitvec)
    {
        std::vector<size_t> listvec;
        listvec.reserve(bitvec.size());
        assign_listvec(listvec, bitvec);
        return listvec;
    }
	
	// assigns {0,1,2,3,4,...,listvec.size()-1} to listvec
	// aka, in bracket notation: [listvec.size()]
	static void assign_identity_map(std::vector<size_t>& listvec)
	{
		for (auto it = listvec.begin(); it != listvec.end(); ++it) {
			*it = size_t(it - listvec.begin());
		}
	}

};

#endif // header

