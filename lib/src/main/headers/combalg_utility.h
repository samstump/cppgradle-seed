// combalg_utility.h

#pragma once
#ifndef combalg_utility_h_29DA250F_8512_4843_8F4E98CF4BCEB88E
#define combalg_utility_h_29DA250F_8512_4843_8F4E98CF4BCEB88E

std::ostream& operator<<(std::ostream& o, const std::vector<bool>& p);
std::ostream& operator<<(std::ostream& o, const std::vector<size_t>& p);

#endif // header
