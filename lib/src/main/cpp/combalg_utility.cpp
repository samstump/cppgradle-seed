// combalg_utility.cpp

#include <vector>
#include <iostream>
#include <chrono>

#include "nanosecond_clock.h"

std::ostream& operator<<(std::ostream& o, const std::vector<bool>& p)
{
    o << '{';
    for (auto it = p.begin(); it != p.end(); ++it) {
        o << *it;
        if (it + 1 < p.end()) o << ", ";
    }
    return o << '}';
}

std::ostream& operator<<(std::ostream& o, const std::vector<size_t>& p)
{
    o << '{';
    for (auto it = p.begin(); it != p.end(); ++it) {
        o << *it;
        if (it + 1 < p.end()) o << ", ";
    }
    return o << '}';
}

#ifdef WINDOWS
win32::nanosecond_clock::time_point win32::nanosecond_clock::now()
{
    uint64_t count;
    QueryPerformanceCounter((LARGE_INTEGER*)&count);
    return time_point(duration(count * static_cast<rep>(period::den) / g_Frequency));
}
#endif
