def score(A):
  S = set()
  P = set()
  low = 0
  for a in A:
    for i in xrange(low, len(A)):
      b = A[i]
#      print a,b
      S.add(a+b)
      P.add(a*b)
    low += 1
  return (S,P,S|P,len(S|P))


def print_score(A,s):
  (S,P,R,z) = s
  print "|A|=",len(A),"A=",A
  print "|S|=",len(S),"S=",S
  print "|P|=",len(P),"P=",P
  print "|R|=",len(R),"R=",R
  print

