#include <algorithm>
#include <iostream>
#include <random>
#include <boost/random.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time/local_time.hpp>

#include "powerset.h"
#include "k_subsets.h"
#include "random_subset.h"
#include "set_adapter.h"
#include "combalg_utility.h"

#include "superkiss64.h"

class cartesian_product_2 {
	public:
		cartesian_product_2(size_t n) : N(n), u(0), v(0), _done(false)
		{
		}
	
		std::pair<size_t, size_t> get_pair() const
		{
			return std::pair<size_t, size_t>(u,v);
		}
	
		void next()
		{
			++v;
			if (v == N) {
				++u;
				if (u == N) {
					_done = true;
				}
				v = u;
			}
		}
		
		bool done() const
		{
			return _done;
		}
		
	private:
		size_t N;
		size_t u;
		size_t v;
		bool _done;
};

size_t lower_bound_r(size_t N)
{
	return (N*N + N) / 4;
}

size_t upper_bound_r(size_t N)
{
	return (N*N + N);
}

std::vector<size_t> compute_RR(const std::vector<size_t>& A)
{
	std::vector<bool> R(A.back()*A.back() + 1, false);
	cartesian_product_2 cp(A.size());

	while (!cp.done()) {
		auto p = cp.get_pair();
		auto x = A[p.first];		// input set is zero based!
		auto y = A[p.second];		// input set is zero based!
		R[x+y] = true;
		R[x*y] = true;
		cp.next();
	}
	return set_adapter::to_listvec(R);
}

std::vector<size_t> compute_R(const std::vector<size_t>& A)
{
	std::vector<bool> R((A.back() + 1)*(A.back() + 1) + 1, false);
	cartesian_product_2 cp(A.size());

	while (!cp.done()) {
		auto p = cp.get_pair();
		auto x = A[p.first] + 1;		// input set is zero based!
		auto y = A[p.second] + 1;		// input set is zero based!
		R[x+y] = true;
		R[x*y] = true;
		cp.next();
	}
	return set_adapter::to_listvec(R);
}

const std::vector<size_t>& offset(std::vector<size_t>& a, int value)
{
	for (auto it = a.begin(); it != a.end(); ++it) {
		*it += value;
	}
	return a;
}

void check_all_k_subsets_of_n(size_t k, size_t n, bool run_on)
{
	
	size_t min_z = upper_bound_r(n);	
	
	std::vector<size_t> base_A = {
//		0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17,19,20,21,22,23,24,25,26,27,29
	};
	if (k <= base_A.size()) {
		std::cout << "error!" << std::endl;
		return;
	}
	size_t nn = n;
	size_t kk = k;
	k = k - base_A.size();
	if (base_A.size() > 0) {
		n = n - base_A.back();
	}
	auto t0 = boost::posix_time::microsec_clock::local_time();
	size_t set_count = 0;
	for (;;) {
		std::cout << "N=" << nn << std::endl;
		all_k_subsets_revdoor_order seq(n,k);
		for (auto it = seq.begin(); it != seq.end(); ++it) {
			std::vector<size_t> x(seq.get_listvec());
			for (auto it = x.begin(); it != x.end(); ++it) {
				*it += base_A.back() + 1;
			}
//			std::cout << "x = " << x << std::endl;
			std::vector<size_t> a(base_A);
			a.insert(a.end(), x.begin(), x.end());
			
			std::vector<size_t> r = compute_R(a);
			++set_count;
			size_t z = r.size();
			if (z < min_z) {
				auto t = boost::posix_time::microsec_clock::local_time();
				auto elapsed = t - t0;
				min_z = z;
				std::cout << std::endl << boost::posix_time::to_iso_string(t) << ": " << z << ": " << offset(a,1) << " : N=" << n << ", K=" << kk << std::endl;
				std::cout << 1.0E6 * set_count / double(elapsed.total_microseconds()) << " sets / usec" << std::endl;
			}
		}
		if (!run_on) {
			break;
		}
		++n;
		++nn;
	}
}

void check_random_k_subsets_of_n(size_t k, size_t n)
{
	std::random_device rd;
	superkiss64 z01(rd(), rd(), rd());
	
	auto t0 = boost::posix_time::microsec_clock::local_time();
	
	size_t min_z = upper_bound_r(n);
	size_t set_count = 0;
	for (;;) {
		std::vector<size_t> a = random_k_subset_listvec(k, n, z01);

		std::vector<size_t> r = compute_R(a);
		++set_count;
		
		size_t z = r.size();
		if (z < min_z) {
			min_z = z;
			auto t = boost::posix_time::microsec_clock::local_time();
			auto elapsed = t - t0;
			std::cout << std::endl << boost::posix_time::to_iso_string(t) << ": " << z << ": " << offset(a,1) << " : N=" << n << ", K=" << k << std::endl;
			std::cout << 1.0e6 * set_count / double(elapsed.total_microseconds()) << " sets / usec" << std::endl;
		}
	}

}	

#define NUM_ARGS 4

int main(int argc, char* argv[])
{
	if (argc != NUM_ARGS) {
		std::cerr << "command k n [rand|enum]";
		return 0;
	}
	size_t k = atoll(argv[1]);
	size_t n = atoll(argv[2]);
	if (strcmp(argv[3], "rand") == 0) {
		check_random_k_subsets_of_n(k,n);
	} else if (strcmp(argv[3], "enum") == 0) {
		check_all_k_subsets_of_n(k,n,false);
	} else if (strcmp(argv[3], "enum+") == 0) {
		check_all_k_subsets_of_n(k,n,true);
	} else {
		return 0;
	}		
}
