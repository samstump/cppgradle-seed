// snp.cpp

#include <algorithm>
#include <iostream>
#include <random>
#include <boost/random.hpp>
#include <chrono>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "powerset.h"
#include "k_subsets.h"
#include "random_subset.h"
#include "set_adapter.h"
#include "combalg_utility.h"


#include "nanosecond_clock.h"

#include "superkiss64.h"
#include "cartesian_product.h"

// |R| <= 2 x |cartesian product of A| = 2 x (n^2+n) / 2 = n^2 + n
size_t upper_bound_R(size_t N)
{
	return (N*N + N);
}

struct candidate_set {
	std::vector<size_t> A;
	std::vector<bool> R;
};

// computes a candidate_set from a smaller candidate_set and an extension set
candidate_set compute_candidate_set(const candidate_set& s, const std::vector<size_t>& extension)
{
	candidate_set newCandidate;
	newCandidate.R.resize(extension.back() * extension.back() + 1, false);
	std::copy(s.R.begin(), s.R.end(), newCandidate.R.begin());
	cartesian_product_2 cp2(extension.size(), s.A.size());
	while (!cp2.done()) {
		auto p = cp2.get_pair();
		auto x = extension[p.first];
		auto y = s.A[p.second];
		newCandidate.R[x+y] = true;
		newCandidate.R[x*y] = true;
		cp2.next();
	}
	cartesian_product_1 cp1(extension.size());
	while (!cp1.done()) {
		auto p = cp1.get_pair();
		auto x = extension[p.first];
		auto y = extension[p.second];
		newCandidate.R[x+y] = true;
		newCandidate.R[x*y] = true;
		cp1.next();
	}
	newCandidate.A.resize(s.A.size() + extension.size());
	std::copy(s.A.begin(), s.A.end(), newCandidate.A.begin());
	std::copy(extension.begin(), extension.end(), newCandidate.A.begin() + s.A.size());
	return newCandidate;
}

// computes the set R as a bit vector from a given set A \subset \Zplus
std::vector<bool> compute_RR_bitvec(const std::vector<size_t>& A)
{
	std::vector<bool> R(A.back()*A.back() + 1, false);
	cartesian_product_1 cp(A.size());

	while (!cp.done()) {
		auto p = cp.get_pair();
		auto x = A[p.first];
		auto y = A[p.second];
		R[x+y] = true;
		R[x*y] = true;
		cp.next();
	}
	return R;
}


// computes the set R as a list vector from a given set A \subset \Zplus
std::vector<size_t> compute_RR(const std::vector<size_t>& A)
{
	return set_adapter::to_listvec(compute_RR_bitvec(A));
}


// computes the set R as a list vector from a given set A of non-negative integers
// ie: elements of A are 0 based such that: {0,2,3} --> {1,3,4}
std::vector<size_t> compute_R(const std::vector<size_t>& A)
{
	std::vector<bool> R((A.back() + 1)*(A.back() + 1) + 1, false);
	cartesian_product_1 cp(A.size());

	while (!cp.done()) {
		auto p = cp.get_pair();
		auto x = A[p.first] + 1;		// input set is zero based!
		auto y = A[p.second] + 1;		// input set is zero based!
		R[x+y] = true;
		R[x*y] = true;
		cp.next();
	}
	return set_adapter::to_listvec(R);
}

// for each element of A, a[i] = a[i] + value
const std::vector<size_t>& offset(std::vector<size_t>& a, int value)
{
	for (auto it = a.begin(); it != a.end(); ++it) {
		*it += value;
	}
	return a;
}

struct leader_entry {
	size_t z;
	// todo: change this to candidate_set?
	std::vector<size_t> A;
	
	leader_entry() : z(0), A() {}
};




// searches all k-element subsets of [n]
// if run_on = true, it restarts and runs for all k-element subsets of [n+1]
void check_all_k_subsets_of_n(size_t k, size_t n, bool run_on)
{
	size_t min_z = upper_bound_R(n);
#ifdef WINDOWS
	typedef win32::nanosecond_clock high_res_timer;
#else
	typedef std::chrono::high_resolution_clock high_res_timer;
#endif
	auto t0 = high_res_timer::now();

	size_t set_count = 0;
	for (;;) {
	//	all_k_subsets_lex_order seq(n,k);
		all_k_subsets_revdoor_order seq(n,k);	// faster than the above by about 30%
		std::vector<size_t> a(seq.get_listvec());
		
		auto t = high_res_timer::now();
		std::chrono::duration<double> elapsed = t - t0;
		std::cout << "K=" << k << ", N=" << n << ", " << set_count / double(elapsed.count()) << " sets/sec" << std::endl;

		
		for (auto it = seq.begin(); it != seq.end(); ++it) {
			std::vector<size_t> a(seq.get_listvec());
			//
			// filter sets here...
			//
			std::vector<size_t> r = compute_R(a);
			++set_count;
			size_t z = r.size();
			if (z < min_z) {
				auto t = high_res_timer::now();
				std::chrono::duration<double> elapsed = t - t0;
				min_z = z;
				std::string timenow = boost::posix_time::to_iso_string(boost::posix_time::second_clock::local_time());
				std::cout << std::endl << timenow << ": " << z << ": " << offset(a,1) << " : N=" << n << ", K=" << k << std::endl;
				std::cout << "R = " << r << std::endl;
				std::cout << set_count / double(elapsed.count()) << " sets/sec" << std::endl;
			}
		}
		if (!run_on) {
			break;
		}
		++n;
	}
}

// random samples from all k-element subsets of [n]
// if use_mt = true, it uses mersenne-twister, otherwise superkiss64
void check_random_k_subsets_of_n(size_t k, size_t n, bool use_mt)
{
	std::random_device rd;
	
	// setup mt (1-3% faster than sk64?)
	boost::mt19937 rng;
	rng.seed(rd());
	boost::uniform_01<boost::mt19937> mt(rng);
	
	// setup sk64
	superkiss64 sk64(rd(), rd(), rd());
	
	auto t0 = boost::posix_time::microsec_clock::local_time();

	// todo: this lambda is a 3% performance drain
	auto z01 = [use_mt, &mt, &sk64]() -> double
	{
		return use_mt ? mt() : sk64();
	};
	
	size_t min_z = upper_bound_R(n);
	size_t set_count = 0;
	for (;;) {
		std::vector<size_t> a = random_k_subset_listvec(k, n, z01);	// about 17% faster than below
		//random_k_subset2(a, k, n, z01);

		std::vector<size_t> r = compute_R(a);
		++set_count;
		
		size_t z = r.size();
		if (z < min_z) {
			min_z = z;
			auto t = boost::posix_time::microsec_clock::local_time();
			auto elapsed = t - t0;
			size_t xn = a.back() + 1;
			std::cout << std::endl << boost::posix_time::to_iso_string(t) << ": " << z << ": " << offset(a,1) << " : N=" << xn << ", K=" << k << std::endl;
			std::cout << 1.0e6 * set_count / double(elapsed.total_microseconds()) << " sets / sec" << std::endl;
		}
	}

}	

// k = starting vector size
// n = max vector size
void fast_search_powerset(size_t k, size_t n)
{
	// enumerates and scores all vectors:
	// with fixed {1,2,3,...k}
	// \cup e \in powerset {k+1,k+2,...,k+n}
	bool verbose = false;
	bool initialize = true;
	
	size_t delta = 5;
	size_t extend_by = 4*delta;
	size_t total_count = (1 << extend_by);
	std::vector<leader_entry> leaders(2000);
	
	
	std::vector<size_t> base(k,0);
	
	size_t d = k;
restart:
	++k;
	//initialize = true;
	if (leaders[k].z == 0) {
		k = d;
	}
	if (initialize) {
		initialize = false;
		base.resize(k,0);
		for (size_t i = 0; i < k; ++i) {
			base[i] = i + 1;
		}
	} else {
		k += delta;
		n += delta;
		base.resize(k,0);
		for (size_t i = 0; i < k; ++i) {
			base[i] = leaders[k].A[i];
		}
		std::cout << "restarting at: k = " << k << ", A = " << base << std::endl;
	}
//	size_t curSize = base.size();
	size_t c = 0;
	//powerset_lex_order(extend_by);
	//powerset_bin_order seq(extend_by);
	powerset_gray_order seq(extend_by);
	for (auto it = seq.begin(); it != seq.end(); ++it) {
		std::vector<size_t> A(base);
		std::vector<size_t> ext(seq.get_listvec());
		for (size_t i = 0; i < ext.size(); ++i) {
			ext[i] += base.back() + 1;
		}
		A.insert(A.end(), ext.begin(), ext.end());
		std::vector<size_t> r = compute_RR(A);
		size_t z = r.size();
		size_t vecSize = A.size();
		if (leaders[vecSize].z == 0 || z < leaders[vecSize].z) {
			leaders[vecSize].z = z;
			leaders[vecSize].A.resize(vecSize);
			std::copy(A.begin(), A.end(), leaders[vecSize].A.begin());
			if (verbose) {
				std::cout << "z = " << r.size() << ", k = " << A.size() << " : " << A << std::endl;
			}
		}
		if (verbose) {
			++c;
			if (c % 10000000 == 0) {
				std::cout << "..." << c << " : " << 100.0 * double(c) / total_count << "%" << std::endl;
			}
			
		}
//		std::cout << "z = " << r.size() << ", k = " << A.size() << " : " << A << std::endl;
	}
	if (verbose) {
		std::cout << "..." << c << " : " << 100.0 * double(c) / total_count << "%" << std::endl;
	}
	for (auto it = leaders.begin(); it != leaders.end(); ++it) {
		if ((*it).z > 0) {
			std::cout << "z=";
			std::cout << std::setw(5) <<  (*it).z;

			size_t tmpk = it - leaders.begin();
			std::cout << ", k=" << tmpk;
			std::cout << ", A = " << (*it).A << std::endl;
		}
	}
	delta = 2;
	goto restart;
}

// k = starting vector size
// n = max vector size
void fast_random_k_subsets(size_t k, size_t n)
{
	size_t extend_by = n - k;

	std::vector<leader_entry> leaders(n+1);
	
	std::vector<size_t> base(k,0);
	for (size_t i = 0; i < k; ++i) {
		base[i] = i + 1;
	}
//	size_t curSize = base.size();
	
	powerset_bin_order seq(extend_by);
	for (auto it = seq.begin(); it != seq.end(); ++it) {
		std::vector<size_t> A(base);
		std::vector<size_t> ext(seq.get_listvec());
		for (size_t i = 0; i < ext.size(); ++i) {
			ext[i] += base.back() + 1;
		}
		A.insert(A.end(), ext.begin(), ext.end());
		std::vector<size_t> r = compute_RR(A);
		size_t z = r.size();
		size_t vecSize = A.size();
		if (leaders[vecSize].z == 0 || z < leaders[vecSize].z) {
			leaders[vecSize].z = z;
			leaders[vecSize].A.resize(vecSize);
			std::copy(A.begin(), A.end(), leaders[vecSize].A.begin());
			std::cout << "z = " << r.size() << ", k = " << A.size() << " : " << A << std::endl;
		}
//		std::cout << "z = " << r.size() << ", k = " << A.size() << " : " << A << std::endl;
	}
	for (auto it = leaders.begin(); it != leaders.end(); ++it) {
		if ((*it).z > 0) {
			std::cout << "k=" << it - leaders.begin() << ", z=" << 
				std::right << std::setw(5) <<  (*it).z << ", A = " << (*it).A << std::endl;
		}
	}
}
