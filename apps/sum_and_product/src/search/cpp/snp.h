// snp.h

#include <stddef.h>

void check_all_k_subsets_of_n(size_t k, size_t n, bool run_on);
void check_random_k_subsets_of_n(size_t k, size_t n, bool use_mt);
void fast_search_powerset(size_t k, size_t n);
void fast_random_k_subsets(size_t k, size_t n);