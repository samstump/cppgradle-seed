// cartesian_product.h

#include "stddef.h"
#include <utility>

// returns A x A, order does not matter.
class cartesian_product_1 {
	public:
		cartesian_product_1(size_t n) : N(n), u(0), v(0), _done(false)
		{
		}
	
		std::pair<size_t, size_t> get_pair() const
		{
			return std::pair<size_t, size_t>(u,v);
		}
	
		void next()
		{
			++v;
			if (v == N) {
				++u;
				if (u == N) {
					_done = true;
				}
				v = u;
			}
		}
		
		bool done() const
		{
			return _done;
		}
		
	private:
		size_t N;
		size_t u;
		size_t v;
		bool _done;
};

// returns A x B, order matters!
class cartesian_product_2 {
	public:
		cartesian_product_2(size_t n, size_t m) : N(n), M(m), u(0), v(0), _done(false)
		{
		}
	
		std::pair<size_t, size_t> get_pair() const
		{
			return std::pair<size_t, size_t>(u,v);
		}
	
		void next()
		{
			++v;
			if (v == M) {
				++u;
				if (u == N) {
					_done = true;
				}
				v = 0;
			}
		}
		
		bool done() const
		{
			return _done;
		}
		
	private:
		size_t N;
		size_t M;
		size_t u;
		size_t v;
		bool _done;
};

