#include <cstdlib>
#include <iostream>
#include <algorithm>

#include "primelist.h"
#include "snp.h"

int less_pair(const std::pair<size_t, size_t>& left, const std::pair<size_t, size_t>& right)
{
    return (left.second > right.second) || ((left.second == right.second) && left.first < right.first);
}


void prime_search(size_t k, size_t n)
{
    const char* primes_filename = "data/primes.bin";
    {
        std::ifstream primes_file(primes_filename);
        if (!primes_file.good()) {
            // regenerate
            std::cout << "regenerate file ..." << std::endl;
        }
    }
    std::cout << "reading: " << primes_filename << std::endl;
    primelist primes(primes_filename);
    std::cout << "done!" << std::endl;
    size_t n_max = n;
    std::vector<size_t> histogram(n_max, 0);

    size_t min_div = -1;
    size_t max_div = 0;
    for (size_t i = 1; i <= n_max; ++i) {
        primelist::factor_result result = primes.factor(i);
        if (result.num_divisors < min_div) {
            min_div = result.num_divisors;
        }
        if (result.num_divisors > max_div) {
            max_div = result.num_divisors;
        }
        size_t num_divisor_pairs = result.num_divisors / 2 + result.num_divisors % 2;
//        ++histogram[result.num_divisors];
        ++histogram[num_divisor_pairs];
//        std::cout << i << '\t' << result.num_divisors << std::endl;
    }
    std::vector<std::pair<size_t, size_t>> h;
    for (auto it = histogram.begin() + min_div; size_t(it - histogram.begin()) <= max_div; ++it) {
        std::pair<size_t, size_t> p(it - histogram.begin(), *it);
        if (p.second != 0) {
            h.push_back(p);
            std::cout << p.first << ": " << p.second << std::endl;
        }
    }

// auto lambda with auto params: needs g++ 4.9
// 'less_pair' functor pattern for g++ 4.8
    auto compare = [](const auto& left, const auto& right) 
    {
        return (left.second > right.second) || 
            ((left.second == right.second) && left.first < right.first);
    };


    std::cout << std::endl;
//    std::sort(h.begin(), h.end(), less_pair);
    std::sort(h.begin(), h.end(), compare);

    for (auto it = h.begin(); it != h.end(); ++it) {
        std::cout << (*it).first << " : " << (*it).second << std::endl;
    }
}

#define NUM_ARGS 4

int main(int argc, char* argv[])
{
    
	if (argc != NUM_ARGS) {
		std::cerr << "command k n [rand0|rand1|enum|enum+|enumpset|randkset|primes]";
		return 0;
	}
	size_t k = atoll(argv[1]);
	size_t n = atoll(argv[2]);
    std::string arg3(argv[3]);
	if (arg3.compare("rand0") == 0) {
		check_random_k_subsets_of_n(k,n,false);
	} else if (arg3.compare("rand1") == 0) {
		check_random_k_subsets_of_n(k,n,true);
	} else if (arg3.compare("enum") == 0) {
		check_all_k_subsets_of_n(k,n,false);
	} else if (arg3.compare("enum+") == 0) {
		check_all_k_subsets_of_n(k,n,true);
	} else if (arg3.compare("enumpset") == 0) {
		fast_search_powerset(k,n);
	} else if (arg3.compare("randkset") == 0) {
		fast_random_k_subsets(k,n);
    } else if (arg3.compare("primes") == 0) {
        prime_search(k, n);
	} else {
		std::cout << "unknown algorithm" << std::endl;
		return 0;
	}
//	std::cout << q.size() << ": (" << l << "," << u << "): " << a << " : " << q << std::endl;	
}
