def score(V,A):
    n = float(len(A))
    return len(V)/(n*(n+1))/2


A = [1,2,3,4,5,6]
P = set()
S = set()
for x in A:
    for y in A:
        S.add(x+y)
        P.add(x*y)
        
print "A=",sorted(A),len(A)
print "P=",sorted(P),len(P),score(P,A)
print "S=",sorted(S),len(S),score(S,A)
PUS = P | S
n = len(A)
print "P U S = ",sorted(PUS),len(PUS),score(PUS,A)
PIS = P & S
print "P I S = ",sorted(PIS),len(PIS)