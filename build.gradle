// build.gradle
//

import org.gradle.internal.os.OperatingSystem;

allprojects {
    apply plugin: "cpp"
    model {
        toolChains {
            gcc(Gcc) {
                eachPlatform { tools ->
                    tools.cppCompiler.withArguments { args -> 
                        if (OperatingSystem.current().isWindows()) {
                            args << "-D WINDOWS"
                        }
                        args << "-g"
                        args << "-m64"
                        args << "-std=gnu++1y"
                        args << "-Wall"
                        args << "-Ofast"
                    }

                    tools.linker.withArguments { args -> 
                        args << "-m64"
                        args << "-lpthread"
                    }
                }
            }

            clang(Clang) {
                eachPlatform { tools ->
                    tools.cppCompiler.withArguments { args -> 
                        args << "-g"
                        args << "-m64"
                        args << "-std=gnu++1y"
                        args << "-Wall"
                        args << "-O3"
                    }
                    tools.linker.withArguments { args -> 
                        args << "-m64"
                    }
                }
            }
        }

        repositories {
            libs(PrebuiltLibraries) {
                googleTest {
                    binaries.withType(StaticLibraryBinary) {
                        def libPath = OperatingSystem.current().isWindows() ? "C:/cygwin64/etc/googletest" : "/etc/googletest"
                        staticLibraryFile = file("${libPath}/libgtest.a")
                        headers.srcDir "${libPath}/include"
                    }
                }
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////
// original latex build credit here: https://gist.github.com/LindsayBradford/11274456
/////////////////////////////////////////////////////////////////////////////////////

def getDviViewerCommand(dviFile)
{
    def dviViewerCommandLine = ''
    if (OperatingSystem.current().isWindows()) {
        // isWindows() seems to mean cygwin.  for example: 'start' is not recognized
        dviViewerCommandLine = 'cygstart ' + dviFile

    } else if (OperatingSystem.current().isLinux()) {
        // equivalent of 'cygstart' or 'start', launch program associated to the file
        dviViewerCommandLine = 'xdg-open ' + dviFile

    } else if (OperatingSystem.current().isMacOsX()) {
        // OS X is the oddball here: I need associate dvi to a real dvi viewer,
        // not a conversion to PDF first, as TeXShop provides.
        def dviViewerArgs = '-s 5 -unique'
        dviViewerCommandLine = 'xdvi ' + dviViewerArgs + ' ' + dviFile
    }
    return dviViewerCommandLine
}

ext { documentBase = 'sum_and_product' }

project.ext {
    latexFile = documentBase + '.tex'
    pdfFile = documentBase + '.pdf'
    dviFile = documentBase + '.dvi'
    
    texToDvi = 'latex'
    texToPdf = 'pdflatex'

    rawDirectory = 'apps/sum_and_product/latex'
    cookedDirectory = 'apps/sum_and_product/build/binaries/latex'
    cookedRelativeToRaw = '../build/binaries/latex'   
}

def createFolderIfNeeded(folder)
{
    File dir = new File(folder)
    if (!dir.exists()) {
        println "Creating folder: $folder"
        dir.mkdirs()
    }
}

task cooktex(type: Exec) {

    doFirst {
        createFolderIfNeeded(cookedDirectory)
        println "Cooking $project.latexFile in $project.cookedDirectory"
    }

    outputs.upToDateWhen {
        
        createFolderIfNeeded(cookedDirectory)
        
        // create a sentinel
        def sentinelFileName = cookedDirectory + '/_pass.1.' + documentBase + '.tmp'
        def sentinel = new File(sentinelFileName)

        // check for the output file
        File outfile = new File(cookedDirectory + '/' + dviFile)
        if (!outfile.exists()) {
            // no output, we are NOT up to date, create sentinel
            sentinel.createNewFile()
            return false;
        }

        // check all .tex and .cls files and compare to output file timestamp ...
        FileTree tree = fileTree(project.rawDirectory) {
            include '**/*.tex'
            include '**/.cls'
        }
        def modified = false
        tree.each { 
            File file ->
                modified = modified || (file.lastModified() > outfile.lastModified())
        }

        // if source is not modified, we are NOT up to date if the sentinel exists
        if (!modified) {
            modified = sentinel.exists()
            sentinel.delete()
        }
        return !modified
    }
  
    workingDir project.rawDirectory
    // uncomment below to hide stdout output of latex command
    // standardOutput = new ByteArrayOutputStream() // stops output to STDOUT
    //
    
    // also available: texToPdf, check on command arguments
    commandLine texToDvi,
        '-output-directory='+project.cookedRelativeToRaw,
        '-halt-on-error',
        '-output-format=dvi',
        project.latexFile

    outputs.file file("$project.cookedDirectory/$project.dviFile")

    doLast {
        println "Cooked $project.latexFile to $project.dviFile"
        //println 'windows='+OperatingSystem.current().isWindows()
        //println 'osx='+OperatingSystem.current().isMacOsX()
        //println 'linux='+OperatingSystem.current().isLinux()

    }
}

// Exec type tasks expect the process to terminate.  Viewers don't necessarily terminate,
// so this task is difficult to put in terms of an Exec type task.
task viewtex(dependsOn: 'cooktex') << {
    getDviViewerCommand("$project.cookedDirectory/$project.dviFile").execute()

}


task flushtex(type: Delete) {
    
    FileTree tree = fileTree(dir: project.cookedDirectory)

    outputs.upToDateWhen {
        tree.isEmpty()
    }
    delete {
        tree
    }
    doLast {
        println "Flushed $project.cookedDirectory"
    }
}
/////////////////////////////////////////////////////////////////////////////////////
project(":lib") {

    model {
        components {
            main(NativeLibrarySpec) {
            }
        }
    }
}

project(":test") {

  apply plugin: "google-test"
  
    model {
        components {
            main(NativeExecutableSpec) {
                sources {
                    cpp {
                        lib project: ':lib', library: 'main', linkage: 'static'
                        lib library: 'googleTest', linkage: 'static'
                    }
                }
            }
        }
    }
}

project(":sum_and_product") {
    model {
        components {
            search(NativeExecutableSpec) {
                sources {
                    cpp {
                        lib project: ':lib', library: 'main', linkage: 'static'
                    }
                }
            }
            test(NativeExecutableSpec) {
                sources {
                    cpp {
                        lib project: ':lib', library: 'main', linkage: 'static'
                    }
                }
            }
        }
    }
}

